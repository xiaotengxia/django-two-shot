from unicodedata import category
from django.shortcuts import render, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.form import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def list_receipt(request):

    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):

    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["category"].queryset = request.user.categories.all()
        form.fields["account"].queryset = request.user.accounts.all()
        # form.fields["category"].owner = request.user
        # form1.category = request.user.categories.all()
        # form1.account = request.user.accounts.all()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def list_account(request):

    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accountlist.html", context)


@login_required
def list_category(request):
    categorylist = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categorylist": categorylist,
    }
    return render(request, "receipts/categorylist.html", context)


@login_required
def create_category(request):
    form = ExpenseCategoryForm()
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    context = {"form": form}
    return render(request, "receipts/createcategory.html", context)


def create_account(request):
    form = AccountForm()
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    context = {
        "form": form,
    }
    return render(request, "receipts/createaccount.html", context)
